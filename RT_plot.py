#!/usr/bin/python3
import sys
import numpy as np
import matplotlib.pyplot as plt

plt.figure()
plt.ion()


for i in range(10):
	y = i**2+1
	plt.scatter(i,y,c='001')
	plt.pause(0.05)

plt.show()
while True:
	plt.pause(0.5)