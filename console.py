#!/usr/bin/python3

import sys,os,time
import struct

class FSM(): # prototype for Finite State Machine
	def __init__(self):
		self.state = 'RESET' 
		self.input = 0 
		self.output = 0 

	def setInput(self):
		pass

	def getOutput(self):
		return self.output

	def getState(self):
		return self.state

	def feed(self,i):
		self.input = i
		self.transit()


	def transit(self):
		pass

	def transverse(self,l):
		for i in range(len(l)):
			self.feed(l[i])

	def run(self):
		self.transit(self.setInput())
		return self.getOutput()

class Console(FSM):
	def __init__(self):
		self.state = 'RESET' # States are RESET,CONFIG,RUN,PAUSE and EXIT
		self.input = 'None' # Input is the text user enters through command line
		self.output = (None,None,None,None) # Output is a tuple (<serial cmd>,<serial data>,<plottor cmd>)
		self.pFlag = False # flag for prompting
		self.l_state = 'RESET'

	def transit(self):
		if(self.state=='RESET'):
			self.prompt()
			if(self.input=='-config'):
				self.state = 'CONFIG'
				self.output = (None,None,None,None)
		elif(self.state=='CONFIG'):
			self.prompt()
			if(self.input=='-run'):
				self.state = 'RUN'
		elif(self.state=='RUN'):
			pass
		elif(self.state=='PAUSE'):
			pass
		elif(self.state=='EXIT'):
			pass

		self.l_state = self.state

	def setInput(self):
		self.input = input(">>> ")

	def prompt(self):
		if(state!=l_state): # if transition happens, prompt the user
			pFlag = False
		if(not self.pFlag):
			pass

def __serial_write(d,b,value=None):
	if(value!=None):
		b += struct.pack('f',value) # assume value is a float number
	d['ser_da'] = b
	# print('b =',b)
	# for i in range(len(b)):
	# 	d['ser_da'] = []
	# 	d['ser_da'].append(b[i])
	# print('in __serial_write,data =',d['ser_da'])
	l_state = d['state']
	print(l_state)
	d['state'] = 'WRITE'
	d['ser_wc'] = False
	while(d['ser_wc']!=True):
		pass
	if(l_state=='RUN'): # if write during RUN or starts with -config, switch to RUN after writing
		d['state'] = 'RUN'
	else:
		d['state'] = 'STOP'


def console(d):
	sys.stdin = os.fdopen(0) # open input flow to allow print on console
	print("Please configure before starting")
	while True:
		time.sleep(0.5)
		cmd = input(">>> ")
		if(cmd == '-exit'):
			__serial_write(d,b'\xf0\x00')
			d['state'] = 'EXIT'
			break

		elif(cmd == '-start'):
			__serial_write(d,b'\xf0\x01')
			d['state'] = 'RUN'

		elif(cmd == '-stop'):
			__serial_write(d,b'\xf0\x00')
			d['state'] = 'STOP'

		elif(cmd in ['-set P','-set p']): # adjust Kp manually
			value = float(input("Enter the value of P: "))
			if (value<1000):
				print('P set to',value)
				__serial_write(d,b'\x01',value)
			else:
				print("invalid value")

		elif(cmd in ['-set I','-set i']): # adjust Ki manually
			value = float(input("Enter the value of I: "))
			if (value<1000):
				print('P set to',value)
				__serial_write(d,b'\x02',value)
			else:
				print("invalid value")

		elif(cmd in ['-set D','-set d']): # adjust Kd manually
			value = float(input("Enter the value of D: "))
			if (value<1000):
				print('P set to',value)
				__serial_write(d,b'\x03',value)
				d['state'] = 'RUN'
			else:
				print("invalid value")

		elif(cmd in ['-config','-Config','--configure']): # config which PID to demonstrate
			while True:
				mode = input("Enter the mode to tune (WATCH,STEP,FREQ): ")
				if(mode=='WATCH'):
					m = b'\xff'
					break
				elif(mode=='STEP'):
					step_halfA =float(input("Enter the half Amplitude of step response: "))
					step_b =struct.pack('f',step_halfA)
					m = b'\xfe'+step_b
					break
				elif(mode=='FREQ'):
					freq_A = float(input("Enter the amplitude for freqency response: "))
					freq_F = float(input("Enter the freqency for freqency response: "))
					freq_b = struct.pack('f',freq_A) + struct.pack('f',freq_F)
					m = b'\xfd'+freq_b
					break
			# while True:
			# 	config = input("Enter the PID loop to tune: ")
			# 	if(config in ['YPP','YSP','PPP','PSP']):
			# 		break
			# 	else:
			# 		print("Valid command: choose from YPP,YPS,PPP,PPS")
			config = d['conf']
			if(config=='YPP'):
				select = b'\x01' #start/stop/mode:[15:8],select:[7:0]
			elif(config=='YSP'):
				select = b'\x02'
			elif(config=='PPP'):
				select = b'\x03'
			elif(config=='PSP'):
				select = b'\x04'
			__serial_write(d,m+select) # tell stm32 to upload relevant data
			#d['plot'] = config # inform plotting to adjust title and such

		elif(cmd=='scale'):
			ref_scale = int(input("Enter scale for ref"))
			fdb_scale = int(input("Enter scale for fdb"))
			out_scale = int(input("Enter scale for out"))
			d['ref_scale'] = ref_scale
			d['fdb_scale'] = fdb_scale
			d['out_scale'] = out_scale
			
		# elif(cmd == '-Pp set'):
		# 	i = int(input("Enter the value of P for Position: "))
		# 	if i in range(0,1000):
		# 		print("Position P set to",i)
		# 		d['ser_da'] = [b'\x01',i] # identification+data
		# 		d['state'] = 'write'
		# 		while(d['ser_wc']!=True):
		# 		time.sleep(0.001)
		# 	d['state'] = 'run'				
		# 	else:
		# 		print("Invalid value")

		# elif(cmd == '-Pi set'):
		# 	i = int(input("Enter the value of I for Position: "))
		# 	if i in range(0,1000):
		# 		print("Position I set to",i)
		# 		d['ser_da'] = [b'\x02',i] # identification+data
		# 		d['state'] = 'write'
		# 		while(d['ser_wc']!=True):
		# 			time.sleep(0.001)
		# 		d['state'] = 'run'
		# 	else:
		# 		print("Invalid value")
					
		# elif(cmd == '-Pd set'):
		# 	i = int(input("Enter the value of D for Position: "))
		# 	if i in range(0,1000):
		# 		print("Position D set to",i)
		# 		d['ser_da'] = [b'\x03',i] # identification+data
		# 		d['state'] = 'write'
		# 		while(d['ser_wc']!=True):
		# 			time.sleep(0.001)
		# 		d['state'] = 'run'
		# 	else:
		# 		print("Invalid value")

		# elif(cmd == '-Sp set'):
		# 	i = int(input("Enter the value of P for Speed: "))
		# 	if i in range(0,1000):
		# 		print("Speed P set to",i)
		# 		d['ser_da'] = [b'\x04',i] # identification+data
		# 		d['state'] = 'write'
		# 		while(d['ser_wc']!=True):
		# 			time.sleep(0.001)
		# 		d['state'] = 'run'
		# 	else:
		# 		print("Invalid value")

		# elif(cmd == '-Si set'):
		# 	i = int(input("Enter the value of I for Speed: "))
		# 	if i in range(0,1000):
		# 		print("Speed I set to",i)
		# 		d['ser_da'] = [b'\x05',i] # identification+data
		# 		d['state'] = 'write'
		# 		while(d['ser_wc']!=True):
		# 			time.sleep(0.001)
		# 		d['state'] = 'run'
		# 	else:
		# 		print("Invalid value")

		# elif(cmd == '-Sd set'):
		# 	i = int(input("Enter the value of D for Speed: "))
		# 	if i in range(0,1000):
		# 		print("Speed D set to",i)
		# 		d['ser_da'] = [b'\x06',i] # identification+data
		# 		d['state'] = 'write'
		# 		while(d['ser_wc']!=True):
		# 			time.sleep(0.001)
		# 		d['state'] = 'run'
		# 	else:
		# 		print("Invalid value")
		else:
			print("Invalid command")
if __name__ == '__main__':
	c = Console()
	while(True):
		print(c.run())
