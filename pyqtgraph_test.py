# from PyQt4 import QtGui
# import pyqtgraph as pg 

# app = QtGui.QApplication([])

# w = QtGui.QWidget()

# btn = QtGui.QPushButton('press me')
# text = QtGui.QLineEdit('enter text')
# plot = pg.PlotWidget()

# layout = QtGui.QGridLayout()
# w.setLayout(layout)

# layout.addWidget(btn,0,0)
# layout.addWidget(text,1,0)
# layout.addWidget(plot,0,1,3,1)

# w.show()

# app.exec_()

import pyqtgraph as pg 
import numpy as np

app = pg.QtGui.QApplication([])
win = pg.GraphicsWindow(title='Testing') # creates a window of GUI
p1 = win.addPlot(title='Plot 1') # p1 is a PlotItem incharge of ploting
p2 = win.addPlot(title='Plot 2') # p2 is another PlotItem in the same window of p1

curve11 = p1.plot() # curve11 is a PlotDataItem displayed in p1(the first PlotItem)
curve12 = p1.plot() # curve12 is another PlotDataItem. Multiple curves in one PlotItem is possible.
data11 = np.linspace(1,10)
curve11.setData(data11,pen='r')
data12 = np.linspace(5,0)
curve12.setData(data12)

scatter1 = p2.plot()
for i in range(100):
	scatter1.addPoints(i**2)

legend1 = p1.addLegend() # add legend to the PlotItem
legend1.addItem(curve11,'red line')
text1 = pg.TextItem('PID parameter')
p1.addItem(text1) # add text to the PlotItem

app.exec_() # start the application