#!/usr/bin/python

import struct

class Convertor:
	def __init__(self):
		self.inputNum = 0
		self.outputNum = 0

	def __byte2float(self,raw):
		"""
		This function translate a byte object to a float. Note that raw must has a size of 4
		"""
		self.inputNum += 4
		self.outputNum += 1
		return struct.unpack('f',raw)[0]

	def byte2float(self,raw,tag):
		"""
		This function takes in a list of raw values and convert them to floats. The floats will be contained
		in a dictionary with keys as specified in tag. Note that length of raw must be 4 time the length of tag
		"""
		if(len(tag)!=len(raw)/4):
			return None
		data = {}
		for i in range(len(tag)):
			data[tag[i]] = __byte2float(raw[4*i:4*(i+1)])
		return data