#!/usr/bin/python3

import multiprocessing as mp 
import time,sys
import console,plotting,serialCOM
import numpy as np 
import pyqtgraph as pg 
from pyqtgraph.Qt import QtGui,QtCore

def console_job(d):
	console.console(d)

def plot_job(q,conf):
	plotting.rt_plot(q,conf)

if __name__ == '__main__':
	m = mp.Manager()
	data_q = mp.Queue() #Queue for passing serial data to real time plotting
	cmd_d = m.dict() #dictionary storing the commands, updated by cmd_pro
	for key in ['state','ser_da','ser_wc','plot']:
		cmd_d[key] = None #initialize dictionary

	while True:
		p = input("Choose the comm port: ")
		if p in ['USB0','USB1','USB2','USB3']:
			port_info = '/dev/tty'+p
			break
		else:
			print("invalid port, choose from USB0 to USB3")

	while True:
		conf = input("Choose which PID to tune: ")
		if conf in ['YPP','YSP','PPP','PSP']:
			cmd_d['conf'] = conf
			break
		else:
			print("invalid configuration, choose from YPP,YSP,PPP and PSP")

	port = serialCOM.serial_init(port_info) #starts the serail port after getting port info from user

	
	cmd_pro = mp.Process(target = console_job,args=(cmd_d,))
	plot_pro = mp.Process(target = plot_job,args=(data_q,conf))
	cmd_pro.start()
	plot_pro.start() #create and start process for command and plotting
	startflag = False
	stopflag = False #flags for prompting program status
	while True: #main loop
		if(cmd_d['state'] == 'RUN'):
			if(not startflag):
				print("Program starts!")
				startflag = True
				stopflag = False #flipping flags to avoid redundant prompting
				serialCOM.serial_flushInput(port) #flush input
			#data_q.put(np.random.random())
			serialCOM.serial_read(port,data_q)
			#serialCOM.serial_speed_test(data_q)
		elif(cmd_d['state'] == 'STOP'):
			if(not stopflag):
				print("Program stopped!")
				stopflag = True
				startflag = False
		elif(cmd_d['state'] == 'WRITE'):
			serialCOM.serial_write(port,cmd_d['ser_da'])
			cmd_d['ser_wc'] = True
			time.sleep(0.01) # delay to let console acknowledge write complete
		elif(cmd_d['state'] == 'EXIT'):
			print("Exiting Program...")
			break
		#time.sleep(0.01)

	cmd_pro.join()
	plot_pro.terminate()
	print("Program terminated")
