#!/usr/bin/python3

import serial,struct
import convertor
import time

rx_buffer = b''
data_buffer = []
r_index = 0

tag = ['YawPp','YawPi','YawPd','YawPref','YawPfdb','YawPout',\
		'YawSp','YawSi','YawSd','YawSref','YawSfdb','YawSout',\
		'PitchPp','PitchPi','PitchPd','PitchPref','PitchPfdb','PitchPout',\
		'PitchSp','PitchSi','PitchSd','PitchSref','PitchSfdb','PitchSout']

class Detector:
	def __init__(self,l):
		self.flag = False
		self.input = b'0'
		self.l_input = b'0'
		self.sequence = l
		self.index = 0

	def feed(self,b):
		if(b == self.sequence[self.index]):
			self.index += 1
		else:
			self.index = 0
		if(self.index==len(self.sequence)):
			self.flag = True

	def if_detect(self):
		return self.flag

	def reset(self):
		self.flag = False
		self.l_input = b'0'
		self.input = b'0'
		self.index = 0



# detector_s = Detector([b'\x03',b'\x04',b'\x03',b'\x04',b'\x03',b'\x04',b'\x03',b'\x04'])
# detector_t = Detector([b'\x05',b'\x06',b'\x05',b'\x06',b'\x05',b'\x06',b'\x05',b'\x06'])
detector_s = Detector([3,4,3,4,3,4,3,4]) # When using buffer, rx_buffer[index] will give an integer
detector_t = Detector([5,6,5,6,5,6,5,6])

cv = convertor.Convertor()
#sequence element must be in b'\xXX' form because b'\x03'==b'3' will evaluate to False

def serial_init(conf):
	port = serial.Serial(conf,baudrate=115200,timeout=1)
	if(not port.is_open):
		port.open()
	port.reset_input_buffer()
	print("serial port initialized!")
	return port

def serial_read(port,q):
	global r_index,rx_buffer
	if(port.inWaiting()!=0):
		rx_buffer += port.read(port.inWaiting())
	if(r_index<len(rx_buffer)):
		inByte = rx_buffer[r_index] # inByte is an integer!
		r_index += 1
		if(not detector_s.if_detect()):#check for starte
			detector_s.feed(inByte)
		else:#if the starter has been detected, read into buffer for process
			#inByte = port.read()
			#data_buffer.append(inByte) #this takes quite long
			detector_t.feed(inByte)#check for terminator
			if(detector_t.if_detect()):#if terminator is detected, process data
				data = rx_buffer[r_index-32:r_index-8] # -32=-24(data)-8(detector)
				# for i in range(96):
				# 	data += [i]
				#print(data)
				#plot_data = serial_data_process(data,q)
				serial_data_process(data,q)
				#print("processing finished")
				#put processed data into data queue, shared by plotting process
				# q.put(plot_data['YawPp'])
				# q.put(plot_data['YawPi']) 
				# q.put(plot_data['YawPd']) 
				# q.put(plot_data['YawPref'])
				# q.put(plot_data['YawPfdb'])
				# q.put(plot_data['YawPout'])
				# q.put(plot_data['YawSp']) 
				# q.put(plot_data['YawSi']) 
				# q.put(plot_data['YawSd']) 
				# q.put(plot_data['YawSref'])
				# q.put(plot_data['YawSfdb'])
				# q.put(plot_data['YawSout'])

				# q.put(plot_data['PitchPp'])
				# q.put(plot_data['PitchPi']) 
				# q.put(plot_data['PitchPd']) 
				# q.put(plot_data['PitchPref'])
				# q.put(plot_data['PitchPfdb'])
				# q.put(plot_data['PitchPout'])
				# q.put(plot_data['PitchSp']) 
				# q.put(plot_data['PitchSi']) 
				# q.put(plot_data['PitchSd']) 
				# q.put(plot_data['PitchSref'])
				# q.put(plot_data['PitchSfdb'])
				# q.put(plot_data['PitchSout'])
				#reset and get ready for next cycle
				detector_s.reset()
				detector_t.reset() #reset both detectors for next cycle

def serial_data_process(data,q):
	#print("processing data")
	# plot_data = {}
	# plot_data['YawPp'] = struct.unpack('f',data[0:4])[0]
	# plot_data['YawPi'] = struct.unpack('f',data[4:8])[0]
	# plot_data['YawPd'] = struct.unpack('f',data[8:12])[0]
	# plot_data['YawPref'] = struct.unpack('f',data[12:16])[0]
	# plot_data['YawPfdb'] = struct.unpack('f',data[16:20])[0]
	# plot_data['YawPout'] = struct.unpack('f',data[20:24])[0]
	# plot_data['YawSp'] = struct.unpack('f',data[24:28])[0]
	# plot_data['YawSi'] = struct.unpack('f',data[28:32])[0]
	# plot_data['YawSd'] = struct.unpack('f',data[32:36])[0]
	# plot_data['YawSref'] = struct.unpack('f',data[36:40])[0]
	# plot_data['YawSfdb'] = struct.unpack('f',data[40:44])[0]
	# plot_data['YawSout'] = struct.unpack('f',data[44:48])[0]
	
	# plot_data['PitchPp'] = struct.unpack('f',data[48:52])[0]
	# plot_data['PitchPi'] = struct.unpack('f',data[52:56])[0]
	# plot_data['PitchPd'] = struct.unpack('f',data[56:60])[0]
	# plot_data['PitchPref'] = struct.unpack('f',data[60:64])[0]
	# plot_data['PitchPfdb'] = struct.unpack('f',data[64:68])[0]
	# plot_data['PitchPout'] = struct.unpack('f',data[68:72])[0]
	# plot_data['PitchSp'] = struct.unpack('f',data[72:76])[0]
	# plot_data['PitchSi'] = struct.unpack('f',data[76:80])[0]
	# plot_data['PitchSd'] = struct.unpack('f',data[80:84])[0]
	# plot_data['PitchSref'] = struct.unpack('f',data[84:88])[0]
	# plot_data['PitchSfdb'] = struct.unpack('f',data[88:92])[0]
	# plot_data['PitchSout'] = struct.unpack('f',data[92:96])[0]

	for i in range(6): # only one set of PID data  is sent (heavy version:24,light version:6)
		q.put(struct.unpack('f',data[4*i:4*(i+1)])[0])
	#supposed sequence: GMYPositionPID->GMYSpeedPID->GMPPositionPID->GMPSpeedPID (heavy version)
	# for key in plot_data.keys():
	# 	print(key,plot_data[key])
	# print('-'*10);[0]
	# return plot_data

def serial_write(port,data):
	port.write(data)
	# for i in range(len(data)):
	# 	port.write(data[i])
	# 	print('sending data',data[i])
	# 	time.sleep(0.0001)
		
def serial_flushInput(port):
	port.reset_input_buffer()

def serial_speed_test(q):
	data = b'\x01'
	data = data*96
	test_data = serial_data_process(data)
	for i in test_data.keys():
		q.put(test_data[i])

if __name__ == '__main__':
	i = 0
