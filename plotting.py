#!/usr/bin/python3

# import multiprocessing as mp 
# import matplotlib.pyplot as plt 
# import time

# def rt_plot(q):
# 	plt.figure()
# 	plt.ion()
# 	time_ms = 0
# 	while True:
# 		if(q.qsize()>=24):
# 			for i in range(24):
# 				plt.scatter(time_ms,q.get())
# 			plt.pause(0.001)
# 			time_ms += 1

import multiprocessing as mp 
import pyqtgraph as pg 
from pyqtgraph.Qt import QtGui, QtCore
import time
from numpy import *
import serialCOM


init_flag = 0
class Plottor:
	def __init__(self,win,s):
		self.p = win.addPlot(title=s)
		self.ref = self.p.plot()
		self.fdb = self.p.plot()
		self.out = self.p.plot()
		self.ref_scale = 1
		self.fdb_scale = 1
		self.out_scale = 1
		self.dict = {} # dictionary for plotting data
		self.counter = 0 # keep track of the X_axis


		self.Kp = 0		
		self.Ki = 0
		self.Kd = 0
		self.Kp_l = 0
		self.Ki_l = 0		
		self.Kd_l = 0 # keep track of PID parameters

		self.paraText = pg.TextItem('P = %f\nI = %f,D = %f' %(self.Kp,self.Ki,self.Kd),anchor=(0,0))
		self.scaleText = pg.TextItem('ref_scale = %d\nfdb_scale = %d\nout_scale = %d\n' %(self.ref_scale,self.fdb_scale,self.out_scale),anchor=(1,0))
		self.p.addItem(self.paraText)
		self.p.addItem(self.scaleText)

		self.legend = self.p.addLegend()
		self.legend.addItem(self.ref,'ref')
		self.legend.addItem(self.fdb,'fdb')
		self.legend.addItem(self.out,'out')

		self.windowWidth = 500
		self.Xm_ref = [0 for i in range(0,self.windowWidth)]
		self.Xm_fdb = [0 for i in range(0,self.windowWidth)]
		self.Xm_out = [0 for i in range(0,self.windowWidth)]
		self.X_axis = [i for i in range(0,self.windowWidth)]
		# self.Xm_ref = linspace(0,0,self.windowWidth)
		# self.Xm_fdb = linspace(0,0,self.windowWidth)
		# self.Xm_out = linspace(0,0,self.windowWidth)
		# self.X_axis = linspace(0,self.windowWidth,num=self.windowWidth)
		self.ptr = 0
		#self.ptr = -self.windowWidth

		#self.app.exec_()

	def update(self,l):
		"""
		l is a list of tsix elements in the order of P,I,D,ref, fdb and output
		"""
		if(self.ptr==self.windowWidth): # clear all data after the window is full
			self.counter += 1
			self.Xm_ref = [0 for i in range(0,self.windowWidth)]
			self.Xm_fdb = [0 for i in range(0,self.windowWidth)]
			self.Xm_out = [0 for i in range(0,self.windowWidth)]
			self.ref.clear()
			self.fdb.clear()
			self.out.clear()			
			# self.Xm_ref = linspace(0,0,self.windowWidth)
			# self.Xm_fdb = linspace(0,0,self.windowWidth)
			# self.Xm_out = linspace(0,0,self.windowWidth)
			#self.X_axis = linspace(self.counter*self.windowWidth,(self.counter+1)*self.windowWidth,num=self.windowWidth)
			self.ptr = 0


		# self.Xm_ref[:-1] = self.Xm_ref[1:]
		# self.Xm_fdb[:-1] = self.Xm_fdb[1:]
		# self.Xm_out[:-1] = self.Xm_out[1:] # shifting data to make room for new data

		#print(self.ptr)
		self.Kp = l[0]
		self.Ki = l[1]
		self.Kd = l[2]
		self.Xm_ref[self.ptr] = l[3]
		self.Xm_fdb[self.ptr] = l[4]
		self.Xm_out[self.ptr] = l[5]/self.Kp # to keep it in same scale
		# self.Xm_ref[self.ptr] = l[3]/self.ref_scale
		# self.Xm_fdb[self.ptr] = l[4]/self.fdb_scale
		# self.Xm_out[self.ptr] = l[5]/self.out_scale
		# self.Xm_ref[-1] = l[3]
		# self.Xm_fdb[-1] = l[4]
		# self.Xm_out[-1] = l[5] # shift method

		# for key in d.keys():
		# 	tag = key[-3::]
		# 	if(tag=='ref'):
		# 		self.Xm_ref[-1] = d[key]
		# 	elif(tag=='fdb'):
		# 		self.Xm_fdb[-1] = d[key]
		# 	elif(tag=='out'):
		# 		self.Xm_out[-1] = d[key]
		if(self.Kp!=self.Kp_l or self.Ki!=self.Ki_l or self.Kd != self.Kd_l):
			self.paraText.setText('P = %f\nI = %f\nD = %f' %(self.Kp,self.Ki,self.Kd)) # update parameters on display if changed
			self.out_scale = self.Kp
			self.scaleText.setText('ref_scale = %d\nfdb_scale = %d\nout_scale = %d\n' %(self.ref_scale,self.fdb_scale,self.out_scale))
		self.Kp_l = self.Kp
		self.Ki_l = self.Ki
		self.Kd_l = self.Kd

		self.ptr += 1 # increment pointer
		self.ref.setData(x=self.X_axis,y=self.Xm_ref,pen='r',clear=True)
		self.fdb.setData(x=self.X_axis,y=self.Xm_fdb,pen='b',clear=True)
		self.out.setData(x=self.X_axis,y=self.Xm_out,clear=True) 
		# self.ref.setData(x=self.X_axis,y=self.Xm_ref,pen='r',clear=True)
		# self.fdb.setData(x=self.X_axis,y=self.Xm_fdb,pen='b',clear=True)
		# self.out.setData(x=self.X_axis,y=self.Xm_out,clear=True)
		# self.ref.setPos(self.ptr,0)
		# self.fdb.setPos(self.ptr,0)
		# self.out.setPos(self.ptr,0)
		QtGui.QApplication.processEvents() # This is necessary!!
		# self.__draw(self.ref,self.Xm_ref)
		# self.__draw(self.fdb,self.Xm_fdb)
		# self.__draw(self.out,self.Xm_out)

	def __draw(self,curve,Xm):
		print('in __draw()...')
		curve.setData(Xm)
		curve.setPos(self.ptr,0)
		QtGui.QApplication.processEvents()

	def set_scale(self,ref=1,fdb=1,out=1):
		self.ref_scale = ref
		self.fdb_scale = fdb
		self.out_scale = out
		self.scaleText.setText('ref_scale = %d\nfdb_scale = %d\nout_scale = %d\n' %(self.ref_scale,self.fdb_scale,self.out_scale))
		self.p.addItem(self.paraText)

def data_process(q):
	d = {}
	for t in serialCOM.tag:
		d[t] = q.get()
	return d

def rt_plot(q,conf):
	app = QtGui.QApplication([])
	win = pg.GraphicsWindow(title='PID smart tune')
	# YPP = Plottor(win,'Yaw Position') # yaw position plottor
	# YSP = Plottor(win,'Yaw Speed'
	p = None
	#plot_l = None
	# d['ref_scale'] = 1
	# d['fdb_scale'] = 1 
	# d['out_scale'] = 1
	#PPP = Plottor(win,'Pitch Position') 
	#PSP = Plottor(win,'Pitch Speed')
	# while True:
	# 	if(d['plot']!=plot_l):
	# 		win.clear()
	if(conf=='YPP'):
		p = Plottor(win,'Yaw Position')
	elif(conf=='YSP'):
		p = Plottor(win,'Yaw Speed')
	elif(conf=='PPP'):
		p = Plottor(win,'Pitch Position')
	elif(conf=='PSP'):
		p = Plottor(win,'Pitch Speed')
			#plot_l = d['plot']
		# if(p != None):
		# 	if(d['ref_scale']!=p.ref_scale or d['fdb_scale'] != p.fdb_scale or d['out_scale'] != p.out_scale):
		# 		p.set_scale(d['ref_scale'],d['fdb_scale'],d['out_scale'])
		#time.sleep(0.001)
	while True:
		if(q.qsize()>=6 and p != None): #[Kp,Ki,Kd,ref,fdb,out]
			data_l = []
			#temp_d = data_process(q)
			for i in range(6): # takes 6 data to draw
				data_l.append(q.get())
			# for key in temp_d.keys():
			# 	print(key+':',temp_d[key])
			#print("qSize =",q.qsize())
			p.update(data_l)
			#YPP.update([temp_d['YawPref'],temp_d['YawPfdb'],temp_d['YawPout']])
			#YPP.update(data_l)
			#YSP.update(data_l[9:12])
	# time_ms = 0
	# while True:
	# 	if(q.qsize()>=24):
	# 		for i in range(24):
	# 			#pg.plot([time_ms],[q.get()])
	# 		time_ms += 1

if __name__ == '__main__':
	# q = []
	# rt_plot(q)
	app = QtGui.QApplication([])
	win = pg.GraphicsWindow(title='PID smart tune')
	#win.addLabel('This is a label',col=1,colspan=4)
	# labelI = pg.LabelItem(text='This is a label')
	# textI = pg.TextItem(text='This is a text',angle=0)
	# win.addItem(labelI)
	# YPP = Plottor(win,'Yaw Position') # yaw position plottor
	# #YSP = Plottor(win,'Yaw Speed')
	# d = {}
	# for i in range(2000):
	# 	d['ref'] = random.random()
	# 	d['fdb'] = random.random()
	# 	d['out'] = random.random()
	# 	#YPP.update([d['ref'],d['fdb'],d['out']])
	# 	YPP.update([i**2,i,i+1,i,i,i])
	# 	#time.sleep(0.01)
	# 	#YSP.update(d)

	p = Plottor(win,'Testing')
	for i in range(1000):
		p.update([10+i,10,10,i,i+1,i-1])
		if(i%100==0 and i!=0):
			p.set_scale(i,i,i)

